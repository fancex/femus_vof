XML_PATH=${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"
# PATH_BC=${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/SRC/User_T.C"
PATH_BC=${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/SRC/"

labels="$(echo "cat /Code_Saturne_GUI/boundary_conditions/boundary/@label " | xmllint --nocdata --shell $XML_PATH | sed '1d;$d')"
names="$(echo "cat /Code_Saturne_GUI/boundary_conditions/boundary/@name" | xmllint --nocdata --shell $XML_PATH | sed '1d;$d')"
natures="$(echo "cat /Code_Saturne_GUI/boundary_conditions/boundary/@nature" | xmllint --nocdata --shell $XML_PATH | sed '1d;$d')"
flags="$(echo "cat /Code_Saturne_GUI/boundary_conditions/boundary/text()" | xmllint --nocdata --shell $XML_PATH | sed '1d;$d')"


i=-1;
for cont in ${labels[*]}; do
    i=$(($i + 1))
    labels_arr[$i]=$cont
done


#names
i=-1;
for cont in ${names[*]}; do
    i=$(($i + 1))
    names_arr[$i]=$cont
done


#natures
i=-1;
for cont in ${natures[*]}; do
    i=$(($i + 1))
    natures_arr[$i]=$cont
done


#flags
i=-1;
for cont in ${flags[*]}; do
    i=$(($i + 1))
    flags_arr[$i]=$cont
done


i=-1;
for cont in ${labels_arr[*]}; do
    i=$(($i + 1))
    if [ ${labels_arr[$i]} != '-------' ]
    then
      echo $i ': label= ' ${labels_arr[$i]:6} ', name= ' ${names_arr[$i]:5} ', nature= ' ${natures_arr[$i]:7} ', flag= ' ${flags_arr[$i]} ', '
    fi
done


#BC converter
#  /* <boundary label="User_TC" name="22" nature="wall">10_354554</boundary>
# // 	<wall label="User_TC">*/
# //BC 
# if( bc_gam==22){ 
#    bc_Neum[0]=10; bc_flag[0]=354554;
#   }




pattern_start='\/\/BC'
pattern_end='\/\/BC_END'

echo  'BC PATH ' $PATH_BC
#delete old boundary condition
i=-1;
for cont in ${labels_arr[*]}; do
    i=$(($i + 1))
    if [ ${labels_arr[$i]} != '-------' ]
    then
	label_local=$( echo ${labels_arr[$i]:10} | sed 's/"//' )
	path_local=$PATH_BC'/'$label_local'.C'
	echo 'clearing' $path_local
	
	sed -i '/'$pattern_start'/,/'$pattern_end'/{//!d}' $path_local
    fi
done

#write new boundary condition
i=-1;
text_bc=''

for cont in ${labels_arr[*]}; do
    i=$(($i + 1))
    if [ ${labels_arr[$i]} != '-------' ]
    then
#       echo $i ': label= ' ${labels_arr[$i]} ', name= ' ${names_arr[$i]} ', nature= ' ${natures_arr[$i]} ', flag= ' ${flags_arr[$i]} ', '
# 	echo ' if ( bc_gam=='${names_arr[$i]:6} ' ){ /n'
# 	echo   '  bc_Neum[0]='${flags_arr[$i]:0:1}'; bc_flag[0]='${flags_arr[$i]:2:1}'; /n'
# 	echo  '} /n'
	 # //BC END ' 
	
	text_bc_local=$(echo "if(bc_gam==atoi(${names_arr[$i]:5}.c_str())){bc_Neum[${flags_arr[$i]:0:1}]=${flags_arr[$i]:2:1};bc_flag[${flags_arr[$i]:0:1}]=${flags_arr[$i]:4:1};}\n")
	text_bc+=$(echo $text_bc_local) 
	#text_bc="if ( bc_gam==${names_arr[$i]:6}  )"
	#text_bc='ciao'
	echo $text_bc_local
 	label_local=$( echo ${labels_arr[$i]:10} | sed 's/"//' )
	path_local=$PATH_BC'/'$label_local'.C'
	echo 'filepath' $path_local
	#sed -i '/'$pattern_start'/,/'$pattern_end'/c\'$pattern_start'\n'$text_bc'\n'$pattern_end'' $PATH_BC/$labels_arr[$i]'.C'
	#sed -i '/\/\/BC/\/\/BC_END/'$text'/' $PATH_BC
	#sed -i '/\/\/BC/,\/\/BC_END/ /\/\/ testttt@' $PATH_BC
	#sed -i '/BEGIN/,/END/c\BEGIN\nfine, thanks\nEND' $PATH_BC
	
	#sed -i 's/'$pattern_start'.*'$pattern_end'/'$pattern_start' foo '$pattern_end'/d' $PATH_BC
	#echo 's/'$pattern_start'/,/'$pattern_end'/'$pattern_start' foo '$pattern_end'/d' $PATH_BC
	
# 	sed -i 's/^'$pattern_end'/' 'ddffbbhf\n'$pattern_end'/' $path_local
	sed -i 's/'$pattern_end'.*/'$text_bc_local'\n'$pattern_end'/' $path_local
    fi
done



