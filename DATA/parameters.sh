#!/bin/sh
echo " # ======================================================= "
echo " / # Numerical Parameters                                  "
echo " # ======================================================  "
echo " #	Config time param                                       "
echo " # ======================================================= "
echo " #	Time:                                       "
   
echo " dt     "\
       `awk 'BEGIN{RS="</time_step_ref>"} /<time_step_ref>/{ gsub(/.*<time_step_ref>/,"");print}' \
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`
echo " itime  "\
       `awk 'BEGIN{RS="</time_ini>"} /<time_ini>/{ gsub(/.*<time_ini>/,"");print}' \
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`
echo " nsteps "  \
       `grep -A 5 '</time_step_ref>' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
      |awk 'BEGIN{RS="</iterations>"} /<iterations>/{ gsub(/.*<iterations>/,"");print}'`

echo " # --------------------------------------------------------"
echo " #   Restart:                                       "

echo " restart " \
       `awk 'BEGIN{RS="</time_restart>"} /<time_restart>/{ gsub(/.*<time_restart>/,"");print}' \
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`
echo " restart_lev  0 " 


echo " # --------------------------------------------------------"
echo " #   Print:                 "

echo " printstep " \
       `awk 'BEGIN{RS="</listing_printing_frequency>"} /<listing_printing_frequency>/{ gsub(/.*<listing_printing_frequency>/,"");print}' \
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`

echo " ndigits 4 "

echo " # ======================================================= "
echo " #	Mesh                                       "
echo " # --------------------------------------------------------"
echo " #	Multigrid:    "


echo " nolevels     "`awk 'BEGIN{RS="</n_lev>"} /<n_lev>/{ gsub(/.*<n_lev>/,"");print}' \
	    ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"` 

echo " moving_mesh  " `grep -c '<ale_method status="on">' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`

echo " # --------------------------------------------------------"
echo " #	Mesh generation:    "

echo " libmesh_gen   " `grep -A 2 '<meshes_list>' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"| grep -c 'RESU' `

echo " mesh_refine	1"

echo " second_order	0"

echo " mgops_gen	1"

echo " ibc_gen	        0"

echo " nintervx         "`awk 'BEGIN{RS="</n_hx>"} /<n_hx>/{ gsub(/.*<n_hx>/,"");print}' \
	    ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`           
echo " nintervy         "`awk 'BEGIN{RS="</n_hy>"} /<n_hy>/{ gsub(/.*<n_hy>/,"");print}' \
	    ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`
echo " nintervz         "`awk 'BEGIN{RS="</n_hz>"} /<n_hz>/{ gsub(/.*<n_hz>/,"");print}' \
	    ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`


echo " # ======================================================= "
echo " #	Parameters              "
echo " # --------------------------------------------------------"
echo " #	Param                      "

echo "  pi	3.14159265359	          "

echo " # --------------------------------------------------------"
echo " / # end Numerical Parameters                            "
echo " # --------------------------------------------------------"
echo "                                                        "
echo "                                                        "
echo " # ======================================================= "
echo " // # Physical Properties"
echo " # ======================================================= "
echo " #	ReferenceValue: "

echo " Uref" \
      `grep -A 5 '<property choice="constant" label="velocity" name="velocity">'\
      ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
     |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " Lref" `grep -A 5 '<property choice="constant" label="Length" name="length">' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
        |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " Tref" `grep -A 5 '<property choice="constant" label="temperature" name="temperature">' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
        |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " Pref" `grep -A 5 '<property choice="constant" label="Pressure" name="pressure">' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
        |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`

echo " # ======================================================= "
echo " #	FluidProperties"
echo " # --------------------------------------------------------"
echo " #  Fluid:" 

echo " rho0   " `grep -A 6 'name="density">' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"|awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " mu0    " `grep -A 6 'name="molecular_viscosity">' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"|awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " kappa0 " `grep -A 6 'name="thermal_conductivity">' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"|awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " cp0    " `grep -A 6 'name="specific_heat">' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"|awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " komp0  " `grep -A 6 'name="compressibility">' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"|awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`

echo " # --------------------------------------------------------"
echo " #  Second phase: "

echo " sigma  0." 
# `grep -A 5 '<property choice="constant" label="velocity" name="velocity">"' \
#${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
#|awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`


echo " # --------------------------------------------------------"
echo " #  Solid:  "

echo " rhos " `grep -A 6 '<property choice="constant" label="Density_s" name="density_s">' \
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
       |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " Es   " `grep -A 6 '<property choice="constant" label="young_modulus" name="young_modulus">' \
        ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
        |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " nis  " `grep -A 6 '<property choice="constant" label="poisson_modulus" name="poisson_modulus">' \
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
       |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " SpecHeat_s  " `grep -A 6 '<property choice="constant" label="SpecHeat_s" name="specific_heat_s">' \
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
       |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " ThermalCond_s  " `grep -A 6 '<property choice="constant" label="ThermalCond_s" name="thermal_conductivity_s">' \
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
       |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`


echo " # ======================================================= "
echo " #	Source term"
echo " # --------------------------------------------------------"
echo " #  Heat source: energy_Q"

echo " qheat " \
       `grep -A 6 '<property choice="constant" label="energy_Q" name="energy_Q">'\
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml" \
       |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " momFx  " `grep -A 6 '<property choice="constant" label="momFx" name="momFx">' \
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
       |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " momFy  " `grep -A 6 '<property choice="constant" label="momFy" name="momFy">' \
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
       |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " momFz  " `grep -A 6 '<property choice="constant" label="momFz" name="momFz">' \
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
       |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`
echo " qs 0 "

echo " # --------------------------------------------------------"
echo " # Gravity: "

echo " dirgx " \
       `grep -A 6 '<property choice="constant" label="momFx" name="momFx">' \
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
       |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`

echo " dirgy " \
       `grep -A 6 '<property choice="constant" label="momFy" name="momFy">'\
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
       |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`

echo " dirgz " \
       `grep -A 6 '<property choice="constant" label="momFz" name="momFz">' \
       ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"\
       |awk 'BEGIN{RS="</initial_value>"} /<initial_value>/{ gsub(/.*<initial_value>/,"");print}'`

echo "# --------------------------------------------------"
echo "// # /Physical Properties "
echo "# --------------------------------------------------"








