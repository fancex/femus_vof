/*-----------------------------------------------------------------------------*  
 *Calcolo delle frazioni volumetriche di una sfera completamente all'interno di*  
 *un grigliato.                                                                *  
 *-----------------------------------------------------------------------------*/  
  #define  NM  65
void dividi(double,double,double,double,double,int,int*,int*,int*,int*,int*);  
  
int conta(double,double,double,double,double);  
double integrale(double,double,double,double,double,int,int);  
  
double fxy(double,double,double);
double fxyz(double,double,double,double);
  
/* double error(double,double,double,double,double,int,int,double,double,double,double);   */
  
/* void inter(double,double,double,double,double *,int*,double,double,int);   */
/* void ord(double *,int *);   */
  
/* void Young(double*,double*,double*,double*); */
/* void normal_smoothing(double[][NM][NM],double[][NM][NM],double[][NM][NM],double[][NM][NM], */
/* 		      int,int,int,double*,double*,double*);   */
/* void Centrale(double [][NM][NM],int,int,int,double*,double*,double*);     */
/* void Best(double*,double*,double*,double*,double*,double*,double*,double*);   */
/* void BestFit1(double*,double*,int,double*,double*,double*);   */
/* void BestFit2(double*,double*,int,double*,double*,double*);   */
/* void BestFit3(double*,double*,int,double*,double*,double*);   */
/* double area(double *p,int lm,double*,double*,double*);   */
  
/* void segno(double*,double*,double*,int*,int*,int*);   */
  
/* void norm(double*,double*,double*);   */
  
/* void termnot(double,double,double,double,double*);   */
void ordina(double*,double*,double*);  
  
  
 
 



#include <math.h>  
#include <stdio.h>  
#include <stdlib.h>  
//#include "Euglib.h"  

//void traslazione(double *a,double *b,int n); 
//void  rotazione(double *a,double *wz,int n); 
//void  rotazione3d(double u[][NM][NM][3],double *wz,int n); 

double c[NM][NM][NM],c1[NM][NM][NM];
FILE *outfile;


int main()  
{  
  float pippo; 
  double x0,y0,z0,r,h;  
  double x,y,z;  
  double mmx,mmy,mmz,tnot,volini,volfin;  
  int n,np;  
  int i,j,k,ji,js,ki,ks,im;   
  int smx,smy,smz,cn,met; 
  //  FILE *pf[6];   
  /*---------------------------------------------- 
    variabili che definiscono il tpo di movimento 
    ----------------------------------------------*/ 
  int tip_mov,d=0,d1,n_step,cnn=1,contatore=1,mov1,NN; 
  //  double vel[3],wz,temp[1000],tp,tt; 
/*   tip_mov=3;  */
/*   vel[0]=0.5;  */
/*   vel[1]=0.5;  */
/*   vel[2]=0.5;  */
/*   wz=0.1;  */
  
  /*------------------------------- 
    inizializzazione della griglia 
    ------------------------------*/ 
  n=NM-1;    np=30;    h=1./n;   NN=1;  
  /*------------------ 
    campo di velocit� 
    -----------------*/ 
 /*  if(tip_mov==1){  */
/*     for(i=0;i<=n_step;i++) temp[i]=1;  */
/*     traslazione( &u[0][0][0][0], &vel[0], n);  */
/*   }  */
/*   if(tip_mov==2){  */
/*     for(i=0;i<=n_step;i++) temp[i]=1;  */
/*     rotazione( &u[0][0][0][0], &wz, n);    */
/*   }  */
/*   if(tip_mov==4){  */
/*     for(i=0;i<=n_step;i++) temp[i]=1;  */
/*     rotazione3d( u, &wz, n);    */
/*   }  */
/*   else if(tip_mov==3) vortex(&u[0][0][0][0], n,&temp[0]); */
  //for(i=1; i<=n_step ;i++) temp[i]=0.;
  /*---------------- 
    funzione colore 
    ---------------*/ 
  r=0.15;    x0=0.5;   y0=0.5;   z0=0.5;  
  /*------------------------------------------------------------*/ 
  outfile=fopen("./c0.in","w");
/*   pf[0]=fopen("visual1/graph.txt","w");  */
/*   pf[1]=fopen("visual1/c.dat","w");  */
/*   pf[2]=fopen("visual1/mx.dat","w");  */
/*   pf[3]=fopen("visual1/my.dat","w");  */
/*   pf[4]=fopen("visual1/mz.dat","w");  */
/*   pf[5]=fopen("visual1/alpha.dat","w");  */
 
 
/*   fprintf(pf[0],"%d\t%d\t%f\t%f\t%f\t%f\t%d\n",2,n,x0,y0,z0,r,n_step);  */
  /*--------------------------------------------------------------*/ 
  dividi (x0,y0,z0,r,h,n,&ki,&ks,&ji,&js,&im); 
     
  for (i=1;i<=n;i++) 
    for (j=1;j<=n;j++) 
      for(k=1;k<=n;k++){ 
	c[i][j][k]=0.;
	c1[i][j][k]=0.;
	x=(i-1)*h-x0; 
	y=(j-1)*h-y0; 
	z=(k-1)*h-z0; 
	cn=conta(x,y,z,r,h); 
	if (cn==27) 
	  c[i][j][k]=1.; 
	else if (cn>0){ 
	  if((k-1)<ki) c[i][j][k]=integrale(x,y,z,r,h,np,-1); 
	  else if((k-1)<ks){ 
	    if ((j-1)<ji) c[i][j][k]=integrale(x,z,y,r,h,np,-1); 
	    else if ((j-1)<js){ 
	      if ((i-1)<=im) c[i][j][k]=integrale(y,z,x,r,h,np,-1); 
	      else c[i][j][k]=integrale(y,z,x,r,h,np,1); 
	    } 
	    else c[i][j][k]=integrale(x,z,y,r,h,np,1); 
	  } 
	  else c[i][j][k]=integrale(x,y,z,r,h,np,1); 
	}
	if(c[i][j][k] > 1.) c[i][j][k]=1.; 
	c1[i][j][k]=c[i][j][k];
      }
   
  // prinrt (header)
  fprintf(outfile," Level %d ",0);
  fprintf(outfile," %d  %d   %d  \n",n,n,n);

  // print data
  volini=0; 
  for(i=1;i<=n;i++)  
    for(j=1;j<=n;j++) 
      for(k=1;k<=n;k++){
	volini+=c[i][j][k];
	fprintf(outfile," %e ",c[i][j][k]);
      } 
  printf("\nvolume iniziale: %f\n",volini);

/*   /\* -----------------------------------  */
/*     calcolo dei coefficienti del piano  */
/*     ----------------------------------*\/  */
/*   met=2; */
/*   mov1=met;  */
/*  metodo:  */
/*   /\*----------------------------------------*\/ */
/*   if(met>=3){  */
/*     for(j=1 ; j<=n ; j++)  */
/*       for(k=1 ; k<=n ; k++){  */
/* 	c[1][j][k]=c[n-1][j][k]; */
/* 	c[n][j][k]=c[2][j][k]; */
/* 	mx[1][j][k]=mx[n-1][j][k]; */
/* 	mx[n][j][k]=mx[2][j][k]; */
/* 	my[1][j][k]=my[n-1][j][k]; */
/* 	my[n][j][k]=my[2][j][k]; */
/* 	mz[1][j][k]=mz[n-1][j][k]; */
/* 	mz[n][j][k]=mz[2][j][k]; */
/* 	alpha[1][j][k]=alpha[n-1][j][k]; */
/* 	alpha[n][j][k]=alpha[2][j][k]; */
/*       }  */
/*     for(i=1 ; i<=n ; i++)  */
/*       for(k=1 ; k<=n ; k++){ */
/* 	c[i][1][k]=c[i][n-1][k]; */
/* 	c[i][n][k]=c[i][2][k]; */
/* 	mx[i][1][k]=mx[i][n-1][k]; */
/* 	mx[i][n][k]=mx[i][2][k]; */
/* 	my[i][1][k]=my[i][n-1][k]; */
/* 	my[i][n][k]=my[i][2][k]; */
/* 	mz[i][1][k]=mz[i][n-1][k]; */
/* 	mz[i][n][k]=mz[i][2][k]; */
/* 	alpha[i][1][k]=alpha[i][n-1][k]; */
/* 	alpha[i][n][k]=alpha[i][2][k]; */
/*       } */
/*     for(i=1 ; i<=n ; i++)  */
/*       for(j=1 ; j<=n ; j++){ */
/*  	c[i][j][1]=c[i][j][n-1]; */
/* 	c[i][j][n]=c[i][j][2]; */
/* 	mx[i][j][1]=mx[i][j][n-1]; */
/* 	mx[i][j][n]=mx[i][j][2]; */
/* 	my[i][j][1]=my[i][j][n-1]; */
/* 	my[i][j][n]=my[i][j][2]; */
/* 	mz[i][j][1]=mz[i][j][n-1]; */
/* 	mz[i][j][n]=mz[i][j][2]; */
/* 	alpha[i][j][1]=alpha[i][j][n-1]; */
/* 	alpha[i][j][n]=alpha[i][j][2]; */
/*       } */
/*     for(i=1;i<=n;i++)  */
/*       for(j=1;j<=n;j++)	  */
/* 	for(k=1;k<=n;k++){  */
/* 	  //if(c[i][j][k]<1.e-10)c[i][j][k]=0.; */
/* 	  //if (c[i][j][k]> 1.-(1.e-10))c[i][j][k]=1.; */
/* 	  mx1[i][j][k]=0.;  */
/* 	  my1[i][j][k]=0.;  */
/* 	  mz1[i][j][k]=0.;  */
/* 	  alpha1[i][j][k]=0.;  */
/* 	}  */
/*   } */
/*   /\*-----------------------------------------------------------*\/ */
/*   for(i=2;i<=n-1;i++)  */
/*     for(j=2;j<=n-1;j++)	  */
/*       for(k=2;k<=n-1;k++){  */
/* 	if(met<=2){  */
/* 	  mx[i][j][k]=0.;  */
/* 	  my[i][j][k]=0.;  */
/* 	  mz[i][j][k]=0.;  */
/* 	  alpha[i][j][k]=0.;  */
/* 	} */
/* 	if(c[i][j][k]>0. && c[i][j][k]<1.){  */
/* 	  if(met==1) Young(&c[i][j][k],&mmx,&mmy,&mmz);  */
/* 	  else if(met==2) Centrale(c,i,j,k,&mmx,&mmy,&mmz);  */
/* 	  else if(met==4 ) normal_smoothing(c,mx,my,mz,i,j,k,&mmx,&mmy,&mmz); */
/* 	  else Best(&c[i][j][k],&mx[i][j][k],&my[i][j][k],&mz[i][j][k], */
/* 		    &alpha[i][j][k],&mmx,&mmy,&mmz); */
	  
/* 	  segno(&mmx,&mmy,&mmz,&smx,&smy,&smz);  */
								 
/* 	  norm(&mmx,&mmy,&mmz);  */
/* 	  if(met<3){ */
/* 	    mx[i][j][k]=mmx*smx;  */
/* 	    my[i][j][k]=mmy*smy;  */
/* 	    mz[i][j][k]=mmz*smz;  */
	    
/* 	    termnot(c[i][j][k],mmx,mmy,mmz,&tnot);  */
	    
/* 	    if(smx==-1) mmx=mx[i][j][k];  */
/* 	    else mmx=0;  */
/* 	    if(smy==-1) mmy=my[i][j][k];  */
/* 	    else mmy=0;  */
/* 	    if(smz==-1) mmz=mz[i][j][k];  */
/* 	    else mmz=0;  */
	    
/* 	    alpha[i][j][k]=tnot+mmx+mmy+mmz; */
/* 	  }  */
/* 	  else if(met>=3){ */
/* 	    mx1[i][j][k]=mmx*smx;  */
/* 	    my1[i][j][k]=mmy*smy;  */
/* 	    mz1[i][j][k]=mmz*smz;  */
	    
/* 	    termnot(c[i][j][k],mmx,mmy,mmz,&tnot);  */
	    
/* 	    if(smx==-1) mmx=mx1[i][j][k];  */
/* 	    else mmx=0;  */
/* 	    if(smy==-1) mmy=my1[i][j][k];  */
/* 	    else mmy=0;  */
/* 	    if(smz==-1) mmz=mz1[i][j][k];  */
/* 	    else mmz=0;  */
	    
/* 	    alpha1[i][j][k]=tnot+mmx+mmy+mmz; */
/* 	  }  */
/* 	}  */
/*       } */

/*   if(met>=3) */
/*     for(i=1;i<=n;i++)  */
/*       for(j=1;j<=n;j++)	  */
/* 	for(k=1;k<=n;k++){  */
/* 	  mx[i][j][k]=mx1[i][j][k];  */
/* 	  my[i][j][k]=my1[i][j][k];  */
/* 	  mz[i][j][k]=mz1[i][j][k];  */
/* 	  alpha[i][j][k]=alpha1[i][j][k];  */
/* 	}  */
/*   if(met == 2 ) {met=3;mov1=met; goto metodo;} */
/*   met=2; */
/*   if( contatore == 1  && ((cnn-1)/(NN*(n)/4)*(NN*(n)/4)) ==(cnn-1) ){        */
/*     //if( contatore == 1  && (cnn-1)!=(cnn-1) ){   */
/*    for(i=1;i<=n;i++)  */
/*      for(j=1;j<=n;j++)  */
/*        for(k=1;k<=n;k++){  */
/* 	 pippo=(float)c[i][j][k];  */
/* 	 fwrite(&pippo,sizeof(float),1,pf[1]);  */
/* 	 pippo=(float)mx[i][j][k];  */
/* 	 fwrite(&pippo,sizeof(float),1,pf[2]);  */
/* 	 pippo=(float)my[i][j][k];  */
/* 	 fwrite(&pippo,sizeof(float),1,pf[3]);  */
/* 	 pippo=(float)mz[i][j][k];  */
/* 	 fwrite(&pippo,sizeof(float),1,pf[4]);  */
/* 	 pippo=(float)alpha[i][j][k];  */
/* 	 fwrite(&pippo,sizeof(float),1,pf[5]);  */
/*        }  */
/*   }  */
/*   if(cnn==n_step+1) goto end;  */
/*   //tp=1.; */
/*   //if(cnn > n_step/2) tp=-1.; */
/*   tp=(1./NN)*cos(acos(-1)*temp[1+((cnn-1)/NN)]/2.); */



/*   if(cnn/6*6==cnn){ */
/*     if (contatore==3){ d=2;d1=2;tt=1;} */
/*     else if (contatore==2) {d=3;d1=1;tt=1;} */
/*     else  {d=1;d1=3;tt=1;} */
/*   } */
/*   else if(cnn/5*5==cnn){ */
/*     if (contatore==3) {d=3;d1=1;tt=1;} */
/*     else if (contatore==2) {d=1;d1=2;tt=1;} */
/*     else   {d=2;d1=1;tt=1;} */
/*   } */
/*   else if(cnn/4*4==cnn){ */
/*     if (contatore==3) {d=1;d1=2;tt=1;} */
/*     else if (contatore==2) {d=2;d1=3;tt=1;} */
/*     else   {d=3;d1=2;tt=1;} */
/*   } */
/*   else if(cnn/3*3==cnn){ */
/*     if (contatore==3) {d=2;d1=2;tt=1;} */
/*     else if (contatore==2) {d=1;d1=3;tt=1;} */
/*     else   {d=3;d1=1;tt=1;} */
/*   } */
/*   else if(cnn/2*2==cnn){ */
/*     if (contatore==3) {d=1;d1=1;tt=1;} */
/*     else if (contatore==2) {d=3;d1=2;tt=1;} */
/*     else   {d=2;d1=3;tt=1;} */
/*   } */
/*   else { */
/*     if (contatore==3) {d=3;d1=2;tt=1;} */
/*     else if (contatore==2) {d=2;d1=1;tt=1;} */
/*     else   {d=1;d1=2;tt=1;} */
/*   } */

/*   if( contatore==3 )  */
/*     lagrange(&u[0][0][0][0],&c[0][0][0],&mx[0][0][0], &my[0][0][0], */
/* 	     &mz[0][0][0],&alpha[0][0][0],n,d,tp);  */
/*   else if( contatore==2)  */
/*     eulerianobalot(&u[0][0][0][0],&c[0][0][0],&mx[0][0][0], &my[0][0][0], */
/* 		   &mz[0][0][0],&alpha[0][0][0],n,d,d1,tp,1.);  */
/*   else{ */
/*     euleriano(&u[0][0][0][0],&c[0][0][0],&mx[0][0][0], &my[0][0][0], */
/* 	      &mz[0][0][0],&alpha[0][0][0],n,d,d1,tp,tt); */
/*   } */
/*   if(contatore== 3){ */
/*     volfin=0;   */
/*     for(i=2;i<=n-1;i++)   */
/*       for(j=2;j<=n-1;j++)  */
/* 	for(k=2;k<=n-1;k++)  */
/* 	  volfin+=c[i][j][k];  */
/*     printf("volume finale: %14.14f  %d\n",volfin,cnn); */
/*   }  */

/*   contatore++;  */
/*   if(contatore==4){  */
/*     contatore=1;  */
/*     cnn++;  */
/*   }  */
/*   goto metodo; */
/*   /\*------------------------------------------------------------------------*\/  */
/*  end:  */
/*   fclose(pf[0]);   */
/*   fclose(pf[1]);   */
/*   fclose(pf[2]);   */
/*   fclose(pf[3]);   */
/*   fclose(pf[4]);   */
/*   fclose(pf[5]); */

/*   pf[0]=fopen("ris.txt","a");  */
/*   volfin=0;   */
/*   for(i=2;i<=n-1;i++)   */
/*     for(j=2;j<=n-1;j++)  */
/*       for(k=2;k<=n-1;k++)  */
/* 	volfin+=c[i][j][k];  */
/*   printf("\nvolume finale: %f\n",volfin);  */
/*   volfin=fabs(volini-volfin)/volini; */
/*   printf("errore relativo: %0.8e\n\n",volfin); */
/*   fprintf(pf[0],"n=%d\t mov=%d\t ric=%d\t CFL=%g\n",n,tip_mov,mov1,1./NN); */
/*   fprintf(pf[0],"errore massa: %0.8e\t",volfin); */
  
  

   
/*   volfin=0; */
/*   for(i=2;i<n;i++) */
/*     for(j=2;j<n;j++) */
/*       for(k=2;k<n;k++) */
/* 	volfin+=fabs(c1[i][j][k]-c[i][j][k]); */
/*   fprintf(pf[0],"errore geometrtrico=%e \n",volfin/volini); */
/*   printf("%e \n",volfin/volini); */
/*   fclose(pf[0]); */
  
/*  return 0;   */
	  
}  
	  
void traslazione(double *a,double *b,int n){ 
  int i,j,k; 
  for(i=0;i<=n;i++) 
    for(j=0;j<=n;j++) 
      for(k=0;k<=n;k++){ 
	  *(a+(NM*NM*3*i)+(NM*3*j)+(3*k))=*b; 
	  *(a+(NM*NM*3*i)+(NM*3*j)+(3*k)+1)=*(b+1); 
	  *(a+(NM*NM*3*i)+(NM*3*j)+(3*k)+2)=*(b+2); 
	} 
  printf("%f\t%f\t%f\n",*b,*(b+1),*(b+2)); 
  return ; 
} 
 
void  rotazione(double *a,double *wz,int n){ 
  int i,j,k; 
  double h; 
 
  h=1./n; 
 
  for(i=0;i<=n;i++) 
    for(j=0;j<=n;j++) 
      for(k=0;k<=n;k++){ 
	*(a+(NM*NM*3*i)+(NM*3*j)+(3*k))=-2*((j-0.5)*h-0.5); 
	*(a+(NM*NM*3*i)+(NM*3*j)+(3*k)+1)=2*((i-0.5)*h-0.5); 
	*(a+(NM*NM*3*i)+(NM*3*j)+(3*k)+2)=0.; 
      } 
  return; 
} 

void  rotazione3d(double u[][NM][NM][3],double *wz,int n){ 
  int i,j,k; 
  double h; 
  double x1,y1,z1,u1,v1;
 
  h=1./n; 
 
  for(i=0;i<=n;i++) 
    for(j=0;j<=n;j++) 
      for(k=0;k<=n;k++){ 

	x1=((i-1)*h-0.5)*1.;
	y1=((j-0.5)*h-0.5)*cos(acos(-1.)/4.)+((k-0.5)*h-0.5)*cos(acos(-1.)/4.);
	z1=((j-0.5)*h-0.5)*cos(acos(-1.)*3./4.)+((k-0.5)*h-0.5)*cos(acos(-1.)/4.);
	u1=-y1;
	v1=x1;
	u[i][j][k][0]=2*u1;
	x1=((i-0.5)*h-0.5)*1.;
	y1=((j-1.)*h-0.5)*cos(acos(-1.)/4.)+((k-0.5)*h-0.5)*cos(acos(-1.)/4.);
	z1=((j-1.)*h-0.5)*cos(acos(-1.)*3./4.)+((k-0.5)*h-0.5)*cos(acos(-1.)/4.);
	u1=-y1;
	v1=x1;
	u[i][j][k][1]=2*v1*cos(acos(-1.)/4.); 
	x1=((i-0.5)*h-0.5)*1.;
	y1=((j-0.5)*h-0.5)*cos(acos(-1.)/4.)+((k-1.)*h-0.5)*cos(acos(-1.)/4.);
	z1=((j-0.5)*h-0.5)*cos(acos(-1.)*3./4.)+((k-1.)*h-0.5)*cos(acos(-1.)/4.);
	u1=-y1;
	v1=x1;
	u[i][j][k][2]=2*v1*cos(acos(-1.)/4.); 

      } 
  return; 
} 
 
 
 
  
/*-----------------------------------------------------------------------*  
 *La funzione "dividi" divide la sfera in 6 parti.                       *  
 *---------------------------------------------------------------------- */  
  
void dividi(double x0,double y0,double z0,double r,double h,int n,  
	    int *ki,int *ks,int *ji,int *js,int *im)  
{  
  int i;  
  double q;  
  
  q=r/2;  
  for (i=0;i<=n;i++){  
    if ((i*h)<=(z0-q))  
      *ki=i;  
    if ((i*h)<(z0+q))  
      *ks=i+1;  
    if ((i*h)<=(y0-q))  
      *ji=i;  
    if ((i*h)<(y0+q))  
      *js=i+1;  
    if ((i*h)<x0)  
      *im=i;  
  }  
  return;  
}  
  
  
/*----------------------------------------------------------------------*  
 *La funzione "conta" conta i punti della cella(i,j,k) all'interno della*  
 *sfera                                                                 *  
 *----------------------------------------------------------------------*/  
  
int conta(double x,double y,double z,double r,double h)  
{  
  int i,j,k,cn=0;  
  double fnz;  
  
  for (i=0;i<=2;i++)  
    for (j=0;j<=2;j++)  
      for (k=0;k<=2;k++){  
	fnz=fxyz((x+i*h/2),(y+j*h/2),(z+k*h/2),r);  
	if (fnz<=0.)  
	  cn++;  
      }  
  return cn;  
}  
  
  
/*---------------------------------------------------------------------*  
 *La funzione "integrale" calcola in modo numerico la porzione di cella*  
 *occupata dalla sfera.                                                *  
 *---------------------------------------------------------------------*/  
  
double integrale(double x,double y,double z,double r,double h,int np,int l)  
{  
  int i,j,mi,mj,cn;  
  double x1,ds,fnz,somma=0;  
  
  ds=h/np;  
  
  x1=x+ds/2;  
  y=y+ds/2;  
  
  for(j=1;j<=np;j++){  
    x=x1;  
    for (i=1;i<=np;i++){  
      fnz=0.;  
      cn=0;  
      if (fxyz(x,y,z,r)<0) cn++;  
      if (fxyz(x,y,z+h,r)<0) cn++;  
      if (cn==2) fnz=2./3;  
      else if (cn==1){  
	if (l==-1) fnz=(fxy(x,y,r)+z+h)*2./(3*h);  
	else fnz=(fxy(x,y,r)-z)*2/(3*h);  
      }  
      for(mi=-1;mi<=1;mi=mi+2)  
	for(mj=-1;mj<=1;mj=mj+2){  
	  fnz+=0.;  
	  cn=0;  
	  if (fxyz(x+mi*ds/2,y+mj*ds/2,z,r)<0) cn++;  
	  if (fxyz(x+mi*ds/2,y+mj*ds/2,z+h,r)<0) cn++;  
	  if (cn==2) fnz+=1./12;  
	  else if (cn==1){  
	    if (l==-1) fnz+=(fxy(x+mi*ds/2,y+mj*ds/2,r)+z+h)/(12*h);  
	    else fnz+=(fxy(x+mi*ds/2,y+mj*ds/2,r)-z)/(12*h);  
	  }  
	}  
      x=x+ds;  
      somma+=fnz;  
    }  
    y=y+ds;  
  }  
  somma=somma/(np*np);  
  return somma;  
}  
  
  
/*------------------------------------------------------------*/  
  
double fxyz(double x,double y,double z,double r)  
{   
  double q;  
  q=x*x+y*y+z*z-r*r;  
  return q;  
}  
  
  
/*--------------------------------------------------------------*/  
  
double fxy(double x,double y,double r)  
{  
  double q;  
  q=sqrt(r*r-x*x-y*y);  
  return q;  
}  
  
  
/*---------------------------------------------------------------------*  
 *La funzione "error" calcola in modo numerico l'errore volumetrico tra*  
 *sfera e piano nella generica cella.                                  *  
 *---------------------------------------------------------------------*/  
  
double error(double x,double y,double z,double r,double h,int np,int l,  
	     double mp,double mq,double mr,double alpha)  
{  
  int i,j,mi,mj,cn;  
  double x1,ds,fnz=0,somma,x0,y0;  
  double p1,dl,fnr,p,q;  
  /*FILE* pf;  
    pf=fopen("errore.txt","a");*/  
  x0=x;  
  y0=y;  
  /*inizio:*/  
  somma=0;  
  ds=h/np;  
  dl=1./np;  
  
  x1=x0+ds/2;  
  y=y0+ds/2;  
  p1=dl/2;  
  q=dl/2;  
  
  for(j=1;j<=np;j++){  
    x=x1;  
    p=p1;  
    for (i=1;i<=np;i++){  
			  
      cn=0;  
      if (fxyz(x,y,z,r)<0) cn++;  
      if (fxyz(x,y,z+h,r)<0) cn++;  
      if (cn==0) fnz=0.;  
      else if (cn==2) fnz=1.;  
      else if (cn==1){  
	if (l==-1) fnz=(fxy(x,y,r)+z+h)/h;  
	else fnz=(fxy(x,y,r)-z)/h;  
      }  
      if(l==-1) fnr=1-(alpha-mp*p-mq*q)/mr;  
      else  fnr=(alpha-mp*p-mq*q)/mr;  
      if (fnr<=0.) fnr=0.;  
      if (fnr>=1.) fnr=1.;  
  
      somma+=(2./3)*fabs(fnz-fnr);  
  
  
      for(mi=-1;mi<=1;mi=mi+2)  
	for(mj=-1;mj<=1;mj=mj+2){  
	  cn=0;  
	  if (fxyz(x+mi*ds,y+mj*ds,z,r)<0) cn++;  
	  if (fxyz(x+mi*ds,y+mj*ds,z+h,r)<0) cn++;  
	  if (cn==0) fnz=0.;  
	  else if (cn==2) fnz=1.;  
	  else if (cn==1){  
	    if (l==-1) fnz=(fxy(x+mi*ds,y+mj*ds,r)+z+h)/h;  
	    else fnz=(fxy(x+mi*ds,y+mj*ds,r)-z)/h;  
	  }  
	  if(l==-1) fnr=1-(alpha-mp*(p+mi*dl)-mq*(q+mj*dl))/mr;  
	  else  fnr=(alpha-mp*(p+mi*dl)-mq*(q+mj*dl))/mr;  
	  if (fnr<=0.) fnr=0.;  
	  if (fnr>=1.) fnr=1.;  
  
	  somma+=(1./12)*fabs(fnz-fnr);  
	}  
      x=x+ds;  
      p=p+dl;  
    }  
    y=y+ds;  
    q=q+dl;  
  }  
  somma=somma/(np*np);  
  /*fprintf(pf,"%14.14f\t",somma);  
    con+=1;  
    if (con<=4){ np*=2; goto inizio;}  
    fprintf(pf,"\n",somma);  
    fclose(pf);*/  
  return somma;  
}  
  
  
/*---------------------------------------------------------------------------*/  
  
void inter(double m1,double m2,double m3,double q,double *p,  
	   int *l,double p1,double p2,int c)  
{  
  double p3;  
  
  if(c==1){  
    p3=(q-m2*p1-m3*p2)/m1;  
    if (0<=p3 && p3<=1){  
      *p=p3;  
      *(p+1)=p1;  
      *(p+2)=p2;  
      (*l)++;  
    }  
  }  
  else if(c==2){  
    p3=(q-m1*p1-m3*p2)/m2;  
    if (0<p3 && p3<1){  
      *p=p1;  
      *(p+1)=p3;  
      *(p+2)=p2;  
      (*l)++;  
    }  
  }  
  else if(c==3){  
    p3=(q-m1*p1-m2*p2)/m3;  
    if (0<p3 && p3<1){  
      *p=p1;  
      *(p+1)=p2;  
      *(p+2)=p3;  
      (*l)++;  
    }  
  }  
  return;  
}  
  
  
/*--------------------------------------------------------------*/  
  
void ord(double *p,int *l)  
{    
  double temp;  
  int i,j,k,m;  
  
  k=*l;  
  i=0;  
 passo1:  
  if(i<k-1){  
    m=0;  
  passo2:  
    j=i+1;		  
  passo3:  
    if( *(p+i*3+m)==1. || *(p+i*3+m)==0.){  
      if(j==k){  
	m++;  
	goto passo2;  
      }  
      while( *(p+j*3+m)!= *(p+i*3+m) ){  
	j++;  
	goto passo3;  
      }  
      if(i+1 != j){  
	temp=*(p+(i+1)*3);  
	*(p+(i+1)*3)=*(p+j*3);  
	*(p+j*3)=temp;	  
	temp=*(p+(i+1)*3+1);  
	*(p+(i+1)*3+1)=*(p+j*3+1);  
	*(p+j*3+1)=temp;	  
	temp=*(p+(i+1)*3+2);  
	*(p+(i+1)*3+2)=*(p+j*3+2);  
	*(p+j*3+2)=temp;  
      }  
      i++;  
      goto passo1;  
    }  
    m++;  
    goto passo2;  
  }  
  (*l)++;  
  k++;  
  *(p+k*3)=*p;  
  *(p+k*3+1)=*(p+1);  
  *(p+k*3+2)=*(p+2);  
  return;  
}  
  
  
  
/*  
  pf=fopen("vol.txt","w");  
  
  for(k=n;k>=1;k--){  
  for(j=n;j>=1;j--){  
  for(i=1;i<=n;i++)  
  if(c[i][j][k]==0.0)  
  fprintf(pf,"%3.2f      ",c[i][j][k]);  
  else if(c[i][j][k]==1.0)  
  fprintf(pf,"%3.4f    ",c[i][j][k]);  
  else  
  fprintf(pf,"%lf  ",c[i][j][k]);  
  
  fprintf(pf,"\n");  
  }  
  fprintf(pf,"\n\n");  
  }  
  
  fclose(pf);  
*/  
  
/*	pf=fopen("mx.txt","w");  
  
	for(k=n-1;k>=2;k--){  
	for(j=n-1;j>=2;j--){  
	for(i=2;i<=n-1;i++)  
	fprintf(pf,"%lf\t",mx[i][j][k]);  
	fprintf(pf,"\n");  
	}  
	fprintf(pf,"\n\n");  
	}  
  
	fclose(pf);*/  
 
