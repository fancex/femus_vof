
PERSONAL=1
PATH_TO=${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/Equations_conf.h"


EQS_FLAG=`awk 'BEGIN{RS="\"/>"} /<ns_eq status=\"/{ gsub(/.*<ns_eq status=\"/,"");print}' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`
if test "$EQS_FLAG" == ""; then
sed -i 's/^ #define  NS_EQUATIONS.*/ \/\/ #define  NS_EQUATIONS/' $PATH_TO
else
sed -i 's/^ \/\/ #define  NS_EQUATIONS/ #define  NS_EQUATIONS/' $PATH_TO
sed -i 's/^ #define  NS_EQUATIONS.*/ #define  NS_EQUATIONS ('$EQS_FLAG')/' $PATH_TO
#//if test "$PERSONAL" == "1"; then
#//echo "     #define NS_IS_PERSONAL "
#//fi
fi

EQS_FLAG=`awk 'BEGIN{RS="\"/>"} /<turb_eq status=\"/{ gsub(/.*<turb_eq status=\"/,"");print}' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`
#if test "$EQS_FLAG" == ""; then
# echo " //  #define  TBK_EQUATIONS                                   "
# else
# echo "     #define TBK_EQUATIONS  ("$EQS_FLAG")"
if test "$EQS_FLAG" == ""; then
sed -i 's/^ #define  TBK_EQUATIONS.*/ \/\/ #define  TBK_EQUATIONS/' $PATH_TO
else
sed -i 's/^ \/\/ #define  TBK_EQUATIONS/ #define  TBK_EQUATIONS/' $PATH_TO
sed -i 's/^ #define  TBK_EQUATIONS.*/ #define  TBK_EQUATIONS ('$EQS_FLAG')/' $PATH_TO
#if test "$PERSONAL" == "1"; then
#echo "     #define TBK_IS_PERSONAL  "
# fi
fi

EQS_FLAG=`awk 'BEGIN{RS="\"/>"} /<en_eq status=\"/{ gsub(/.*<en_eq status=\"/,"");print}' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`
if test "$EQS_FLAG" == ""; then
sed -i 's/^ #define  T_EQUATIONS.*/ \/\/ #define  T_EQUATIONS/' $PATH_TO
else
sed -i 's/^ \/\/ #define  T_EQUATIONS/ #define  T_EQUATIONS/' $PATH_TO
sed -i 's/^ #define  T_EQUATIONS.*/ #define  T_EQUATIONS ('$EQS_FLAG')/' $PATH_TO
#if test "$PERSONAL" == "1"; then
#echo "     #define T_IS_PERSONAL "
#fi
fi

EQS_FLAG=`awk 'BEGIN{RS="\"/>"} /<en_turb_eq status=\"/{ gsub(/.*<en_turb_eq status=\"/,"");print}' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`
# if test "$EQS_FLAG" == ""; then
# echo " //   #define TTBK_EQUATIONS                               "
# else
# echo "      #define  TTBK_EQUATIONS  ("$EQS_FLAG")"
if test "$EQS_FLAG" == ""; then
sed -i 's/^ #define  TTBK_EQUATIONS.*/ \/\/ #define  TTBK_EQUATIONS/' $PATH_TO
else
sed -i 's/^ \/\/ #define  TTBK_EQUATIONS/ #define  TTBK_EQUATIONS/' $PATH_TO
sed -i 's/^ #define  TTBK_EQUATIONS.*/ #define  TTBK_EQUATIONS ('$EQS_FLAG')/' $PATH_TO

# if test "$PERSONAL" == "1"; then
# echo "     #define TTBK_IS_PERSONAL "
# fi
fi

EQS_FLAG=`awk 'BEGIN{RS="\"/>"} /<sm_eq status=\"/{ gsub(/.*<sm_eq status=\"/,"");print}' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`
if test "$EQS_FLAG" == ""; then
# echo " //  #define SM_EQUATIONS                                 "
# else
# echo "     #define SM_EQUATIONS   ("$EQS_FLAG")"
sed -i 's/^ #define  SM_EQUATIONS.*/ \/\/ #define  SM_EQUATIONS/' $PATH_TO
else
sed -i 's/^ \/\/ #define  SM_EQUATIONS/ #define  SM_EQUATIONS/' $PATH_TO
sed -i 's/^ #define  SM_EQUATIONS.*/ #define  SM_EQUATIONS ('$EQS_FLAG')/' $PATH_TO
fi
                                                        
EQS_FLAG=`awk 'BEGIN{RS="\"/>"} /<neu_eq status=\"/{ gsub(/.*<neu_eq status=\"/,"");print}' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`
if test "$EQS_FLAG" == ""; then
# echo " //  #define NEU_EQUATIONS                                 "
# else
# echo "     #define NEU_EQUATIONS   ("$EQS_FLAG")"
sed -i 's/^ #define  NEU_EQUATIONS.*/ \/\/ #define  NEU_EQUATIONS/' $PATH_TO
else
sed -i 's/^ \/\/ #define  NEU_EQUATIONS/ #define  NEU_EQUATIONS/' $PATH_TO
sed -i 's/^ #define  NEU_EQUATIONS.*/ #define  NEU_EQUATIONS ('$EQS_FLAG')/' $PATH_TO
fi
                                                            
EQS_FLAG=`awk 'BEGIN{RS="\"/>"} /<elec_eq status=\"/{ gsub(/.*<elec_eq status=\"/,"");print}' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`
if test "$EQS_FLAG" == ""; then
# echo " //  #define V_EQUATIONS                                 "
# else
# echo "     #define V_EQUATIONS   ("$EQS_FLAG")"
sed -i 's/^ #define  V_EQUATIONS.*/ \/\/ #define  V_EQUATIONS/' $PATH_TO
else
sed -i 's/^ \/\/ #define  V_EQUATIONS/ #define  V_EQUATIONS/' $PATH_TO
sed -i 's/^ #define  V_EQUATIONS.*/ #define  V_EQUATIONS ('$EQS_FLAG')/' $PATH_TO
fi

EQS_FLAG=`awk 'BEGIN{RS="\"/>"} /<fsi_eq status=\"/{ gsub(/.*<fsi_eq status=\"/,"");print}' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`
if test "$EQS_FLAG" == ""; then
# echo " //  #define FSI_EQUATIONS                                 "
# else
# echo "     #define FSI_EQUATIONS   ("$EQS_FLAG")"
# if test "$PERSONAL" == "1"; then
# echo "     #define FSI_IS_PERSONAL "
# fi
sed -i 's/^ #define  FSI_EQUATIONS.*/ \/\/ #define  FSI_EQUATIONS/' $PATH_TO
else
sed -i 's/^ \/\/ #define  FSI_EQUATIONS/ #define  FSI_EQUATIONS/' $PATH_TO
sed -i 's/^ #define  FSI_EQUATIONS.*/ #define  FSI_EQUATIONS ('$EQS_FLAG')/' $PATH_TO
fi

EQS_FLAG=`awk 'BEGIN{RS="\"/>"} /<mhd_eq status=\"/{ gsub(/.*<mhd_eq status=\"/,"");print}' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`
if test "$EQS_FLAG" == ""; then
# echo " // #define MHD_EQUATIONS                                 "
# else
# echo "    #define MHD_EQUATIONS   ("$EQS_FLAG")"
sed -i 's/^ #define  MHD_EQUATIONS.*/ \/\/ #define  MHD_EQUATIONS/' $PATH_TO
else
sed -i 's/^ \/\/ #define  MHD_EQUATIONS/ #define  MHD_EQUATIONS/' $PATH_TO
sed -i 's/^ #define  MHD_EQUATIONS.*/ #define  MHD_EQUATIONS ('$EQS_FLAG')/' $PATH_TO
fi

EQS_FLAG=`awk 'BEGIN{RS="\"/>"} /<vof_eq status=\"/{ gsub(/.*<vof_eq status=\"/,"");print}' ${FEMUS_DIR}"/USER_APPL/"${FM_MYAPP}"/DATA/case1.xml"`
if test "$EQS_FLAG" == ""; then
# echo " // #define TWOPH_EQUATIONS                                 "
# else
# echo "    #define TWOPH_EQUATIONS   ("$EQS_FLAG")"
sed -i 's/^ #define  TWOPH_EQUATIONS.*/ \/\/ #define  TWOPH_EQUATIONS/' $PATH_TO
else
sed -i 's/^ \/\/ #define  TWOPH_EQUATIONS/ #define  TWOPH_EQUATIONS/' $PATH_TO
sed -i 's/^ #define  TWOPH_EQUATIONS.*/ #define  TWOPH_EQUATIONS ('$EQS_FLAG')/' $PATH_TO
fi
                                             
echo " end Equations_conf.sh"






























