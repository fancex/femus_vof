// *****************************************************************
//      MGGeomElconf
// *****************************************************************

#ifndef _quadtri_cfg
#define _quadtri_cfg

// ======= GEOMETRIC ELEMENT TYPE ====
//Geometric element,
// only quadratic,
// volume and boundary

//here no structure, but the numbers

#include "Domain_conf.h"


// Type of element (27-> Hex) (10-> tetra)
#define ELTYPE 27        //quadrilateral
// #define ELTYPE 10     //triangular 


//***********

#define NUMGEOMELS 1//=========number of geometric element types

// ============================================================================
#if ELTYPE == 27
// ================== ELTYPE 27 (EXAEDRAL-QUAD-EDGE) ==========================
// ============================================================================
#if DIMENSION==2   // ---------- 2D -------------------------------------------
  #define  NNDS        (9)                          //Lagrange mg-Quad9 name
  #define  NNDS_B      (3)                          //Lagrange mg-Edge3 name
  #define  MED_EL_TYPE INTERP_KERNEL::NORM_QUAD9    //Lagrange med-QUAD9 name
  #define  MED_EL_BDTYPE INTERP_KERNEL::NORM_SEG3   //Lagrange med-edge3 name
  // Lagrange Quad9-Quad4(P)-Edge3(B)-Edge2(PB)
  #define  NDOF_FEM    (9)        // Lagrange Quad9(Volume) dofs
  #define  NDOF_FEMB   (3)        // Lagrange Edge3(Boundary) dofs
  #define  NDOF_P      (4)        // Lagrange Quad4(Volume linear P) dofs
  #define  NDOF_PB     (2)        // Lagrange Edge2(Boundary linear PB) dofs
  #define  NSUBDOM     (4)        // Lagrange Quad9 ->  Quad4
#endif
#if DIMENSION==3 // ---------- 3D ---------------------------------------------
  #define  NNDS       (27)                          //Lagrange mg-hex27  name
  #define  NNDS_B      (9)                          //Lagrange mg-Quad9  name
  #define  MED_EL_TYPE INTERP_KERNEL::NORM_HEXA27   //Lagrange med-hex27 name
  #define  MED_EL_BDTYPE INTERP_KERNEL::NORM_QUAD9  //Lagrange med-QUAD9 name
  // Lagrange hex27-hex8(P)-quad9(B)-quad4(PB)
  #define  NDOF_FEM    (27)       // Lagrange hex27 (Volume) dofs
  #define  NDOF_FEMB   (9)        // Lagrange Quad9 (Boundary) dofs
  #define  NDOF_P      (8)        // Lagrange hex8 (Volume linear P) dofs
  #define  NDOF_PB     (4)        // Lagrange quad4 (Boundary linear PB) dofs
  #define  NSUBDOM     (8)        // Lagrange hex27 ->  hex8  
#endif
// ============================================================================
#endif
// =============================(EXAEDRAL-QUAD-EDGE) ==========================
// ============================================================================

// ============================================================================
#if ELTYPE == 10
// ================== ELTYPE 10 (TETRA-TRI-EDGE) ==============================
// ============================================================================
#if DIMENSION==2 // -----------------------------------------------------------
 #define  NNDS        (6)                           //Lagrange mg-Tri6 name
 #define  NNDS_B      (3)                           //Lagrange mg-Edge3 name
 #define  MED_EL_TYPE INTERP_KERNEL::NORM_TRI6      //Lagrange med-Tri6 name
 #define  MED_EL_BDTYPE INTERP_KERNEL::NORM_SEG3    //Lagrange med-Edge3 name
 // Lagrange Tri6-Tri3(P)-Edge3(B)-Edge2(PB)
 #define  NDOF_FEM    (6)              // Lagrange Tri6(Volume) dofs
 #define  NDOF_FEMB   (3)              // Lagrange Edge3(Boundary) dofs
 #define  NDOF_P      (3)              // Lagrange Tri3(Volume linear P) dofs
 #define  NDOF_PB     (2)              // Lagrange Edge2(Boundary linear PB) dofs
 #define  NSUBDOM     (4)              // Lagrange Tri6 ->  Tri3
  
#endif

#if DIMENSION==3  // ----------------------------------------------------------- 
 #define  NNDS       (10)                         //Lagrange TETRA0
 #define  NNDS_B      (6)                         //Lagrange TRI6
 #define  MED_EL_TYPE INTERP_KERNEL::NORM_TETRA10 //Lagrange MED-TETRA0
 #define  MED_EL_BDTYPE INTERP_KERNEL::NORM_TRI6  //Lagrange MED-TRI6
 // Lagrange Tetra10-Tetra4(P)-Tri6(B)-TRI3(PB)
 #define  NDOF_FEM    (10)             // Lagrange Tetra10 (Volume) dofs
 #define  NDOF_FEMB   (6)              // Lagrange Tri6 (Boundary) dofs
 #define  NDOF_P      (4)              // Lagrange Tetra4 (Volume linear P) dofs
 #define  NDOF_PB     (3)              // Lagrange TRI3(Boundary linear PB) dofs
 #define  NSUBDOM     (8)              // Lagrange Tetra10 -> Tetra4
#endif
// ============================================================================
#endif
// ======================== (TETRA-TRI-EDGE) ==================================
// ============================================================================


 
// *****************************************************************
//      MGFEconf
// *****************************************************************

#define  Q2Q1               //quadratic-linear
#define  NDOF_K      (1)
#define  NDOF_BK      (0)

//  #define  Q2Q0           //quadratic-piecewise
// #define  NDOF_K      (1)
// #define  Q2P1  
// #define  NDOF_K      (3) //quadratic-3piecewise


#endif   // end header

